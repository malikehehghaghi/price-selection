package com.example.priceselection;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;

import com.example.priceselection.adapter.ImageAdapter;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    String[] amounts = {
            "۵۰٫۰۰۰ ریال",
            "۱۰۰٫۰۰۰ ریال",
            "۲۰۰٫۰۰۰ ریال",
            "۵۰۰٫۰۰۰ ریال",
            "۱٫۰۰۰٫۰۰۰ ریال",
            "۱٫۲۰۰٫۰۰۰ ریال",
            "۱٫۵۰۰٫۰۰۰ ریال"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        updateResources(MainActivity.this, "fa");
        Button myButton = (Button) findViewById(R.id.submit);
        EditText myText = (EditText) findViewById(R.id.amount);
        ImageAdapter adapter = new ImageAdapter(this, amounts);
        ListView listView = (ListView) findViewById(R.id.myList);
        listView.setAdapter(adapter);
        setListViewHeightBasedOnChildren(listView);
        Typeface font = Typeface.createFromAsset(getAssets(), "ir_sans.ttf");
        myButton.setTypeface(font);
        myText.setTypeface(font);
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ActionBar.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    private static void updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(locale);
        } else {
            configuration.setLocale(locale);
        }

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }


}
