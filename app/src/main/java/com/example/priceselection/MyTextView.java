package com.example.priceselection;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.Toast;

/**
 * Created by malikeh on 2017-08-31.
 */

public class MyTextView extends android.support.v7.widget.AppCompatTextView {
    public MyTextView(Context context) {
        super(context);
        Typeface iranSans = Typeface.createFromAsset(context.getAssets(), "ir_sans.ttf");
        this.setTypeface(iranSans);
    }

    public MyTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "ir_sans.ttf");
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MyTextView);
        String fontName = a.getString(R.styleable.MyTextView_fontName);

        if (!TextUtils.isEmpty(fontName))
            typeFace = Typeface.createFromAsset(context.getAssets(), fontName);
        this.setTypeface(typeFace);
    }

}
