package com.example.priceselection.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.priceselection.MyTextView;
import com.example.priceselection.R;

/**
 * Created by malikeh on 2017-08-26.
 */

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    private String[] amountList;

    public ImageAdapter(Context c, String[] amountList) {
        mContext = c;
        this.amountList = amountList;
    }

    public ImageAdapter getAdapter() {
        return this;
    }

    public int getCount() {
        return amountList.length;
    }

    public String getItem(int position) {
        return amountList[position];
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.list_item, parent, false);
        MyTextView amount = (MyTextView) rowView.findViewById(R.id.amount);
        amount.setText(amountList[position]);
        return rowView;
    }


}
